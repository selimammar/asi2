import { createSlice } from '@reduxjs/toolkit'

export const cardSlice = createSlice({
  name: 'Card',
  // Define initial state of the reducer/slice
  initialState: {
    user_cardList: [],
    shop_cardList:{},
    current_card:{},
  },
  // Define the reducers 
  reducers: {
    updateShopCardList: (state,action) =>{
        state.shop_cardList = action.payload
    },
    updateUserCardList: (state,action) =>{
        state.user_cardList = action.payload
    },
    updateCurrentCard: (state,action) =>{
        state.current_card = action.payload
    },
    addCard: (state,action) =>{
        state.user_cardList.push(action.payload)
    }
}
})

// Action creators are generated for each case reducer function
export const { updateShopCardList,updateUserCardList,updateCurrentCard,addCard } = cardSlice.actions

export default cardSlice.reducer