import { createSlice } from '@reduxjs/toolkit'

export const TitleReducer = createSlice({
    name: 'Title',
    // Define initial state of the reducer/slice
    initialState: {
        current_title: "Home",
    },
    // Define the reducers 
    reducers: {
        update_title: (state, action) => {
            state.current_title = action.payload
            console.log(state.current_title)
        },
    }
})

// Action creators are generated for each case reducer function
export const { update_title } = TitleReducer.actions

export default TitleReducer.reducer