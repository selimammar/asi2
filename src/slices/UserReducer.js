import { createSlice } from '@reduxjs/toolkit'

export const UserReducer = createSlice({
  name: 'User',
  // Define initial state of the reducer/slice
  initialState: {
    current_user: {},
  },
  // Define the reducers 
  reducers: {
    update_user: (state, action) => {
        state.current_user = action.payload
    },
}
})

// Action creators are generated for each case reducer function
export const { update_user} = UserReducer.actions

export default UserReducer.reducer
