import React,{ useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import { Login_Register } from './components/body/content/Login_Register/Login_Register.jsx'
import { useDispatch, useSelector } from "react-redux";
import './App.css'
import { HeaderDisplay } from './components/header/header'
import {Content} from './components/body/content/Content.jsx'
import * as jsonUser from './sources/cards_user.json';
import * as jsonShop from './sources/cards_shop.json';
import { addCard, updateShopCardList } from './slices/cardSlice.js';
import { updateUserCardList } from './slices/cardSlice.js';
import { update_title } from './slices/TitleReducer.js';
import { FeedLabel } from 'semantic-ui-react';


function App() {

  const [shop_card_list, setShopCards] = useState();
  const [user_card_list, setuserCards] = useState();
  const dispatch = useDispatch();
  fetch('http://tp.cpe.fr:8083/cards_to_sell')
  .then(response => response.json())
  .then(json => dispatch(updateShopCardList(json)))

  const user = useSelector(state => state.userReducer.current_user)
  if(user.id != undefined && user.id != null && user.id != "" && user.id !=0){
    fetch('http://tp.cpe.fr:8083/user/'+user.id)
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Something went wrong');
    })
    .then((responseJson) => {
      if (responseJson.cardList.length != 0) {
        //TODO : change foeach to for loop 
        for (let index = 0; index < responseJson.cardList.length; index++) {
          fetch(`http://tp.cpe.fr:8083/card/${responseJson.cardList[index]}`)
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('Something went wrong');
          })
          .then((responseJson) => {
            console.log(responseJson)
            dispatch(addCard(responseJson))
          })
          .catch((error) => {
            console.log(error)
          });
        }
      }
    })
  }
  

  return (
    <>
      <header>
        <HeaderDisplay/>
      </header>
      <body>
        <div className="content">
          <Content>
          </Content>
        </div>
      </body>
    </>
  );
}

export default App;
