import { configureStore } from '@reduxjs/toolkit';
import userReducer from './slices/UserReducer';
import titleReducer from './slices/TitleReducer';
import cardSlice from './slices/cardSlice';

export default configureStore({
    reducer: {
        userReducer: userReducer,
        titleReducer: titleReducer,
        cardReducer: cardSlice,
    },
})