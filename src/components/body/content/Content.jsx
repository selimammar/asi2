import React, { Component } from 'react';
import {CardList} from './card_list/CardList';
import { useSelector } from 'react-redux';
import { Login_Register } from './Login_Register/Login_Register';
import {Buttons} from './home_buttons/Buttons';

export const Content = (props) =>{

    function getDisplayContent(){
        let display_content;
        let title = useSelector(state => state.titleReducer.current_title)
        if(title == "Sell" | title == "Buy"){
            display_content = <CardList/>;
        }
        else if(title == "Login/Register"){
            display_content = <Login_Register></Login_Register>;
        }
        else if (title == "Home"){
            display_content = <Buttons></Buttons>
        }
        else{
            display_content = <>Error 404 :(</>
        }
        return display_content
    }

    let display_content= getDisplayContent();

        return (
            <div>
                {display_content}
            </div>
        )

}