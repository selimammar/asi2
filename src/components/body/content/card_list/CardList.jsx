import React, { Component } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { CardTab } from '../../../Card/CardTab';

export const CardList = (props) =>{

    function getAllCardRender(){
        let array_render=[];
        let title = useSelector(state => state.titleReducer.current_title);
        
        if(title == "Buy"){ {/* partie achat */}

            let shop_card_list = useSelector(state => state.cardReducer.shop_cardList)
            for(var i=0;i<shop_card_list.length;i++){
            array_render.push(
                <CardTab
                    id = {i}
                    Card={shop_card_list[i]}
                />
                );
            }
        }

        else if(title == "Sell"){ {/* partie vente */}

            let user_card_list = useSelector(state => state.cardReducer.user_cardList)
            for(var i=0;i<user_card_list.length;i++){
            array_render.push(
                <CardTab
                    id = {i}
                    Card={user_card_list[i]}
                />
                );
            }
        }
        return array_render;
    }

    const display_list= getAllCardRender();

        return (
            <div>
                <table className="ui selectable celled table" id="cardListId">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Family</th>
                            <th>HP</th>
                            <th>Energy</th>
                            <th>Defence</th>
                            <th>Attack</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {display_list}
                    </tbody>
                </table>
            </div>      
        )

}