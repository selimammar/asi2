import React from "react";
import { useDispatch } from "react-redux";
import { update_title } from "../../../../slices/TitleReducer";

export const Buttons = (props) =>{

    function play(){
        dispatch(update_title("Play"))
    }

    function sell(){
        dispatch(update_title("Sell"))
    }

    function buy(){
        dispatch(update_title("Buy"))
    }



    const dispatch = useDispatch()

    return (
        <div className="buttons-container">
            <div className="buy-sell-buttons">
              <button className="action-button" onClick={buy}> <img className="icon" src="/src/images/buy.jpg" alt="Buy Icon"/> Buy </button>
              <button className="action-button" onClick={sell}> <img className="icon" src="/src/images/sell.jpg" alt="Sell Icon" /> Sell </button>
            </div>
            <button className="action-button" onClick={play}> <img className="icon" src="/src/images/play_game.png" alt="Play game Icon"/> Play Game </button>
        </div>
    )
}