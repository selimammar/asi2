import React,{ useState } from 'react'
import { Grid, Segment, Input, Button} from 'semantic-ui-react'
import { User } from '../../../user/containers/User'
import { UserFormRegister } from '../../../userform/UserFormRegister'
import {UserFormLogin} from '../../../userform/UserFormLogin'
import './Login_Register.css'

export const Login_Register = (props) =>{

  const [showLoginForm, setShowLoginForm] = useState(true);

  const toggleLoginForm = () => {
    setShowLoginForm(!showLoginForm);
    };

  function handleChange(data){
  };


  return (
    <>
        <div>
          <Grid centered>
            <Grid.Column mobile={16} tablet={8} computer={6} className="form-container">
              <Segment>
                {!showLoginForm ? (
                  <UserFormRegister
                    handleChange={handleChange}>
                  </UserFormRegister>
                ) : (
                  <UserFormLogin
                    handleChange={handleChange}>
                  </UserFormLogin>
                )}
                {showLoginForm ? (
                  <p>
                    Create an account? Register <span onClick={toggleLoginForm} className="trigger-word">Here</span>
                  </p>
                ) : (
                  <p>
                    Already have an account? Login <span onClick={toggleLoginForm} className="trigger-word">Here</span>
                  </p>
            )}
              </Segment>
            </Grid.Column>
          </Grid>
        </div>
    </>
  )
}