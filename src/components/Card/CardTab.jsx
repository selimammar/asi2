import React, { Component } from 'react';
import { useSelector } from 'react-redux';
export const CardTab =(props) =>{

    
    let title = useSelector(state => state.titleReducer.current_title)
    return (
                <tr key={props.id}>
                    <td>
                        <img  className="ui avatar image" src={props.Card.img}/> <span>{props.Card.name}</span>
                    </td>
                    <td>{props.Card.desc}</td>
                    <td>{props.Card.family}</td>
                    <td>{props.Card.HP}</td>
                    <td>{props.Card.energy}</td>
                    <td>{props.Card.defence}</td>
                    <td>{props.Card.attack}</td>
                    <td>{props.Card.price}</td>
                    <td>
                        <div className="ui vertical animated button" tabIndex="0">
                            <div className="hidden content">{title}</div>
                            <div className="visible content">
                                <i className="shop icon"></i>
                            </div>
                        </div>
                    </td>
                </tr>       
        );

}
