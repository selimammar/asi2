import React, { useState } from 'react';
import { Form, Header,Button } from 'semantic-ui-react'
import { useDispatch } from 'react-redux';
import { update_user } from '../../slices/UserReducer';
import { update_title } from '../../slices/TitleReducer';
import { current } from 'immer';

export const UserFormLogin = (props) =>{
       const [currentUser,setCurrentUser]= useState({
                                            login:"",
                                            pwd:"",
                                        });

    function processInput(event, { valueData }){
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(event.target.value);
        let currentVal=currentUser;
        setCurrentUser({...currentUser, [name]: value});
        currentVal[name]= value;
        props.handleChange(currentVal);
    };

    function submitOrder(data){
        const  user = {
            "username":currentUser.login,
            "password":currentUser.pwd,
        }
        console.log(user)
        fetch('http://tp.cpe.fr:8083/auth',{method: 'POST',body : JSON.stringify(user),headers: {'Content-Type': 'application/json'}})
        .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('Something went wrong');
          })
          .then((responseJson) => {
            dispatch(update_user({login : user.username, pwd : user.password,id: responseJson}))
            dispatch(update_title("Home"))
          })
          .catch((error) => {
            console.log(error)
          });

          
    }
    const dispatch = useDispatch()

    return (
        <Form>
            <Header as='h2' dividing>
                User Login
            </Header>
            <Form.Field>
                <Form.Input label="Login" placeholder="" className='form-field' onChange={processInput}  name="login" value={currentUser.login}/>
            </Form.Field>
            <Form.Field>
                <Form.Input type="password" label="Password" className='form-field' placeholder="" onChange={processInput}  name="pwd" value={currentUser.pwd}/>
            </Form.Field>
            <Button type='submit' onClick={submitOrder} className='submit-button'>Sign in</Button>
        </Form>
        

    );
    
    }
