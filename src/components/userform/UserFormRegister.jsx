import React, { useState } from 'react';
import { Form, Header,Button } from 'semantic-ui-react'
import './UserForm.css'
import { useDispatch, useSelector } from 'react-redux';
import { update_user } from '../../slices/UserReducer';
import { update_title } from '../../slices/TitleReducer';


export const UserFormRegister = (props) =>{
    const [currentUser,setCurrentUser]= useState({
                                        surname:"",
                                        lastname:"",
                                        login:"",
                                        pwd:"",
                                        email:"",
                                        valid_pwd:"",
                                    });

    function processInput(event, { valueData }){
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(event.target.value);
        let currentVal=currentUser;
        setCurrentUser({...currentUser, [name]: value});
        currentVal[name]= value;
        props.handleChange(currentVal);
    };

    function submitOrder(){
        const user = {
            id:0,
            account:0,
            listcart:[],
            surName:currentUser.surname,
            lastName:currentUser.lastname,
            login:currentUser.login,
            pwd:currentUser.pwd,
            email:currentUser.email,
        }
        fetch('http://tp.cpe.fr:8083/user',{method: 'POST',body : JSON.stringify(user),headers: {'Content-Type': 'application/json'}})
        .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('Something went wrong');
          })
          .then((responseJson) => {
            dispatch(update_user({json : responseJson.id, login : responseJson.login, pwd : responseJson.pwd}))
            dispatch(update_title("Home"))
          })
          .catch((error) => {
            console.log(error)
          });
    };
    const dispatch = useDispatch();

    return (
        <Form onSubmit={submitOrder}>
            <Header as='h2' dividing>
                User Registration
            </Header>
            <Form.Group widths='equal'>
                <Form.Input fluid label='Surname' placeholder='' className='form-field' name="surname" onChange={processInput} value={currentUser.surname} />
                <Form.Input fluid label='Last Name' placeholder='' className='form-field' name="lastname"  onChange={processInput} value={currentUser.lastname}/>
            </Form.Group>

            <Form.Field>
                <Form.Input label="Login" placeholder="" className='form-field' onChange={processInput}  name="login" value={currentUser.login}/>
            </Form.Field>
            <Form.Field>
                <Form.Input label="Email" placeholder="" className='form-field' onChange={processInput}  name="email" value={currentUser.email}/>
            </Form.Field>
            <Form.Field>
                <Form.Input type="password" label="Password" className='form-field' placeholder="At least 6 characters" onChange={processInput}  name="pwd" value={currentUser.pwd}/>
            </Form.Field>
            <Form.Field>
                <Form.Input type="password" label="Re-enter password" className='form-field' placeholder="" onChange={processInput}  name="valid_pwd" value={currentUser.valid_pwd}/>
            </Form.Field>
            <Button type='submit' className='submit-button'>Create Account</Button>          
        </Form>

    );
    
    }
