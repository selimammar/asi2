import React from 'react';
import './Buttons.css';

const Buttons = () => {
  return (
    <div className="buttons-container">
      <div className="buy-sell-buttons">
        <button className="action-button">
          <img className="icon" src="/src/assets/images/buy.jpg" alt="Buy Icon" />
          Buy
        </button>
        <button className="action-button">
          <img className="icon" src="/src/assets/images/sell.jpg" alt="Sell Icon" />
          Sell
        </button>
      </div>
      <button className="action-button">
        <img className="icon" src="/src/assets/images/play_game.png" alt="Play game Icon" />
        Play Game
      </button>
    </div>
  );
};

export default Buttons;
