import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { update_title } from "../../../slices/TitleReducer";


export const UserDisplay=(props) =>{

    function goToLogin(){
        dispatch(update_title("Login/Register"))
    }
    let display = []
    const dispatch = useDispatch()
    const user = useSelector(state => state.userReducer.current_user)
    console.log(user)


    if(user.login === undefined){
        display.push(  
        <div>
            <button onClick={goToLogin}>Login</button>
        </div>) 
    }
    else{
        display.push(
            <div>
              Bienvenue {user.login} !
            </div>)
    }
    return(
        <div className="headerUser">
            {display}  
        </div>
    )
}