import React from "react";
import { render } from "react-dom";
import { LeftSideDisplay } from "./LeftSide/leftside";
import { UserDisplay } from "./user/user";
import { Title } from "./Title/title";


export const HeaderDisplay=(props) =>{
    return(
        <header className="header">
            <LeftSideDisplay/>
            <Title></Title>
            <UserDisplay photo={props.photo} name={props.name}/>
        </header>
    )
}