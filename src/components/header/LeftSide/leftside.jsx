import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { update_title } from "../../../slices/TitleReducer";


export const LeftSideDisplay=(props) =>{

    function goToHome(){
        dispatch(update_title("Home"))
    }

    const dispatch = useDispatch();
    
    let title = useSelector(state => state.titleReducer.current_title)
    /*title update with a reducer*/ 
    return(
        <div className="headerLeftSide">
            <img src='src\images\playing_cards.png' alt="logo" className="icon" onClick={goToHome}/>
        </div>
    )
}