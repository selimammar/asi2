import React from "react";
import { useSelector } from "react-redux";

export const Title=(props) =>{    

    let title = useSelector(state => state.titleReducer.current_title)
    return(
        <div className="headerTitle">
            <h2 className="Title">{title}</h2>
        </div>
    )
}