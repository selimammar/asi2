package com.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.cpe.springboot.user.model.UserDTO;

@Service
public class BusService {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMsg(UserDTO user) {
        System.out.println("[BUSSERVICE] SEND String MSG=["+user+"]");
        jmsTemplate.convertAndSend("RESULT_BUS_MNG",user);
    }

    public void sendMsg(UserDTO user, String busName) {
        System.out.println("[BUSSERVICE] SEND String MSG=["+user+"] to Bus=["+user+"]");
        jmsTemplate.convertAndSend(busName,user);
    }
}


