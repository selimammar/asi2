# ASI2 - Atelier 1

## Contributors : 
- Ammar Selim
- Baillivet Chloé
- Messié Louis
- Perreyon Thomas

## Activités réalisées par personne :

Selim Ammar : Interface Accueil (Home) (front), appels API, architecture.
Chloé Baillivet : Header (front), connexion des différents composants (front - redux), appels API, architecture.
Louis Messié : Interface Vente/Achat de cartes,  connexion des différents composants (front - redux), Springboot, architecture.
Thomas Perreyon : Login/Register (front), architecture.

## Éléments réalisés :

- Interface Frontend (page d'accueil, achat/vente de carte, login/register, header)
- Springboot (bus de communication)
- L'architecture actuelle et la transformation en microservices

